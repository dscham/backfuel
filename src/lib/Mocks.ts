import Trip from "./model/trip/Trip";
import IdGenerator from "./IdGenerator";
import Distance from "./model/measurement/Distance";
import DistanceUnit from "./model/measurement/DistanceUnit";
import Consumption from "./model/measurement/Consumption";
import ConsumptionUnit from "./model/measurement/ConsumptionUnit";

export function generateTrips(count: number) {
    const trips: Trip[] = [];
    for (let i = 0; i < count; i++) {
        const distance = randomInt(1, 600);
        trips.push(new Trip(
            IdGenerator.generateId(10),
            randomDate(new Date(2012, 0, 1), new Date()),
            new Distance(distance, DistanceUnit.KILOMETER),
            new Consumption(randomConsumption(distance), ConsumptionUnit.LITER))
        );
    }
    return trips;
}

function randomDate(start: Date, end: Date): Date {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function randomInt(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomConsumption(fromDistance: number) {
    return parseFloat(((fromDistance / 100) * randomFloat(1, 12, 3)).toFixed(3));

    function randomFloat(min: number, max: number, toFixed: number) {
        return parseFloat((Math.random() * (max - min) + min).toFixed(toFixed));
    }
}