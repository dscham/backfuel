import Refuel from "./model/refuel/Refuel";
import Price from "./model/measurement/Price";
import Consumption from "./model/measurement/Consumption";

export enum RefuelStorageEvent {
    ADD = 'add',
    REMOVE = 'remove',
    UPDATE = 'update',
    CLEAR = 'clear',
    OVERWRITE = 'overwrite',
}

export class RefuelStorage {
    private static readonly storageKey = 'refuels';
    private static readonly storage = window.localStorage;

    private static callbacks: Function[] = [];

    public static subscribe(callback: (changedRefuel: Refuel | Refuel[], event?: RefuelStorageEvent) => void): void {
        RefuelStorage.callbacks.push(callback);
    }

    public static unsubscribe(callback: () => void): void {
        const index = RefuelStorage.callbacks.indexOf(callback);
        if (index === -1) {
            return;
        }
        RefuelStorage.callbacks.splice(index, 1);
    }

    public static getRefuels(): Refuel[] {
        const refuels = RefuelStorage.storage.getItem(RefuelStorage.storageKey);
        if (!refuels) {
            return [];
        }
        let parsedRefuels = JSON.parse(refuels);
        return parsedRefuels.map((refuel: any) => new Refuel(refuel.id, new Date(refuel.time), new Consumption(refuel.consumption.value, refuel.consumption.unit), new Price(refuel.price.value, refuel.price.unit)));
    }

    public static addRefuel(refuel: Refuel): void {
        const refuels = RefuelStorage.getRefuels();
        refuels.push(refuel);
        RefuelStorage.storage.setItem(RefuelStorage.storageKey, JSON.stringify(refuels));
        RefuelStorage.callbacks.forEach((callback) => callback(refuel, RefuelStorageEvent.ADD));
    }

    public static removeRefuel(refuel: Refuel): void {
        const refuels = RefuelStorage.getRefuels();
        const index = refuels.findIndex((t) => t.id === refuel.id);
        if (index === -1) {
            return;
        }
        refuels.splice(index, 1);
        RefuelStorage.storage.setItem(RefuelStorage.storageKey, JSON.stringify(refuels));
        RefuelStorage.callbacks.forEach((callback) => callback(refuel, RefuelStorageEvent.REMOVE));
    }

    public static updateRefuel(refuel: Refuel): void {
        const refuels = RefuelStorage.getRefuels();
        const index = refuels.findIndex((t) => t.id === refuel.id);
        if (index === -1) {
            return;
        }
        refuels[index] = refuel;
        RefuelStorage.storage.setItem(RefuelStorage.storageKey, JSON.stringify(refuels));
        RefuelStorage.callbacks.forEach((callback) => callback(refuel, RefuelStorageEvent.UPDATE));
    }

    public static clear(): void {
        RefuelStorage.storage.removeItem(RefuelStorage.storageKey);
        RefuelStorage.callbacks.forEach((callback) => callback(undefined, RefuelStorageEvent.CLEAR));
    }

    public static getRefuel(id: string): Refuel | undefined {
        const refuels = RefuelStorage.getRefuels();
        return refuels.find((t) => t.id === id);
    }

    public static getRefuelIndex(id: string): number {
        const refuels = RefuelStorage.getRefuels();
        return refuels.findIndex((t) => t.id === id);
    }

    public static getRefuelCount(): number {
        return RefuelStorage.getRefuels().length;
    }

    public static overwriteRefuels(refuels: Refuel[]): void {
        RefuelStorage.storage.setItem(RefuelStorage.storageKey, JSON.stringify(refuels));
        RefuelStorage.callbacks.forEach((callback) => callback(refuels, RefuelStorageEvent.OVERWRITE));
    }
}