export default class IdGenerator {
    static generateId(length: number): string {
        const values = new Uint8Array(length);
        window.crypto.getRandomValues(values);
        let id = "";
        for (const element of values) {
            id += element.toString(36);
        }
        return id;
    }
}