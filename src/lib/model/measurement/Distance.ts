import type Measurement from "../common/Measurement";
import DistanceUnit from "./DistanceUnit";

export default class Distance implements Measurement {
    value: number = 0;
    unit: DistanceUnit = DistanceUnit.KILOMETER;

    constructor(value?: number, unit?: DistanceUnit) {
        this.value = value ? value : this.value;
        this.unit = unit ? unit: this.unit;
    }
}