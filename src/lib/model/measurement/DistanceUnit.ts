export default class DistanceUnit {
    static readonly KILOMETER: DistanceUnit = new DistanceUnit('Kilometer', 'km');
    static readonly MILE: DistanceUnit = new DistanceUnit('Mile', 'mi');

    static get values(): DistanceUnit[] {
        return [
            DistanceUnit.KILOMETER,
            DistanceUnit.MILE,
        ];
    }

    private constructor(public readonly name: string, public readonly label: string) { }
}