import type Measurement from "../common/Measurement";
import ConsumptionUnit from "./ConsumptionUnit";

export default class Price implements Measurement {
    value: number = 0;
    unit: ConsumptionUnit = ConsumptionUnit.LITER;

    constructor(value?: number, unit?: ConsumptionUnit) {
        this.value = value ? value : this.value;
        this.unit = unit ? unit: this.unit;
    }
}