export default class ConsumptionUnit {
    static readonly LITER: ConsumptionUnit = new ConsumptionUnit('Liter', 'l');
    static readonly GALLON: ConsumptionUnit = new ConsumptionUnit('Gallon', 'gal');

    static get values(): ConsumptionUnit[] {
        return [
            ConsumptionUnit.LITER,
            ConsumptionUnit.GALLON,
        ];
    }

    private constructor(public readonly name: string, public readonly label: string) { }
}