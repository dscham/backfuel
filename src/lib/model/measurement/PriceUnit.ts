export default class PriceUnit {
    static readonly EURO: PriceUnit = new PriceUnit('Euro', '€');
    static readonly DOLLAR: PriceUnit = new PriceUnit('Dollar', '$');

    static get values(): PriceUnit[] {
        return [
            PriceUnit.EURO,
            PriceUnit.DOLLAR,
        ];
    }

    private constructor(public readonly name: string, public readonly label: string) { }
}