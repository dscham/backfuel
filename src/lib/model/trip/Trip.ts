import type DateRange from "../common/DateRange";
import Distance from "../measurement/Distance";
import Consumption from "../measurement/Consumption";

export default class Trip {
    id: string;
    time: Date | DateRange;
    duration?: number;
    distance: Distance;
    consumption: Consumption;


    constructor(id?: string, time?: Date | DateRange, distance?: Distance, consumption?: Consumption, duration?: number) {
        this.id = id ? id : "";
        this.time = time ? time : new Date();
        this.distance = distance ? distance : new Distance();
        this.consumption = consumption ? consumption : new Consumption();
        this.duration = duration;
    }
}