import type DistanceUnit from "../measurement/DistanceUnit";
import type ConsumptionUnit from "../measurement/ConsumptionUnit";

export default interface Measurement {
    value: number;
    unit: ConsumptionUnit | DistanceUnit;
}