import type DateRange from "../common/DateRange";
import Distance from "../measurement/Distance";
import Consumption from "../measurement/Consumption";
import Price from "../measurement/Price";
import IdGenerator from "../../IdGenerator";

export default class Refuel {
    id: string;
    time: Date;
    consumption: Consumption;
    price: Price;


    constructor(id?: string, time?: Date, consumption?: Consumption, price?: Price) {
        this.id = id ? id : IdGenerator.generateId(10);
        this.time = time ? time : new Date();
        this.price = price ? price : new Price();
        this.consumption = consumption ? consumption : new Consumption();
    }
}